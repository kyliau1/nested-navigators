import { View, Text, Pressable } from 'react-native'
import React from 'react'
import { useRouter } from 'expo-router'

const monkey = () => {
  const router = useRouter()

  return (
    <View>
      <Text>stacks</Text>
      <Pressable onPress={() => router.push('/(tabs)/cupboards/cupboard1')}>
        <Text>
          Go to cupboard 1
        </Text>
      </Pressable>
      <Pressable onPress={() => router.push('/(tabs)/cupboards/cupboard2')}>
        <Text>
          Go to cupboard 2
        </Text>
      </Pressable>
    </View>
  )
}

export default monkey